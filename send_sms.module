<?php

/**
 * @file
 * Drupal hook implementations and helpers.
 */

/**
 * Default message type.
 */
define('SEND_SMS_TYPE_TEXT', 'SMS:TEXT');

/**
 * Url of the remote sending server.
 */
define('SEND_SMS_API_URL', 'https://212.124.121.186/api');

/**
 * A special flag that indicates that the recipient of the SMS is not
 * directly knows or does not matter. In this case the SMS will be sent
 * to the number that is configured through the UI.
 */
define('SEND_SMS_DEFAULT_RECIPIENT', '');

/**
 * Sends an SMS to the provided phone number.
 *
 * @param string $phone
 *   Desired recipient of the message. Phone number must be in an international
 *   format. Or SEND_SMS_DEFAULT_RECIPIENT to send to the default number.
 *   Multiple recipients may be separated by commas; in this case the same SMS
 *   is send to every recipient individually.
 *
 * @param string $text
 *   Text of the message. Can be at most 32,000 characters long.
 *   Must be in UTF-8.
 *
 * @return bool
 *   Returns FALSE if it encounters a problem or error.
 */
function send_sms_send($phone, $text) {
  if ($phone === SEND_SMS_DEFAULT_RECIPIENT) {
    $phone = send_sms_get_default_recipient();
  }

  $phones = array();
  foreach (explode(',', $phone) as $p) {
    $p = preg_replace('/[^0-9]/', '', trim(strval($p)));
    if ($p && preg_match('/^7[0-9]{10}$/', $p)) {
      $phones[] = $p;
    }
  }
  if (empty($phones) || empty($text)) {
    return FALSE;
  }

  $login = variable_get('send_sms_api_username');
  $password = variable_get('send_sms_api_password');
  if (empty($login) || empty($password)) {
    return FALSE;
  }

  foreach ($phones as $phone) {
    _send_sms_envoy($phone, $text, $login, $password);
  }
  return TRUE;
}

/**
 * Private function.
 *
 * @see send_sms_send()
 */
function _send_sms_envoy($phone, $text, $login, $password, $mtype = SEND_SMS_TYPE_TEXT) {
  $originator = _send_sms_get_originator($phone);

  $query = drupal_http_build_query(array(
    'action' => 'sendmessage',
    'username' => $login,
    'password' => $password,
    'recipient' => $phone,
    'messagetype' => $mtype,
    'originator' => $originator,
    'messagedata' => $text,
  ));

  $response = drupal_http_request(SEND_SMS_API_URL, array(
    'method' => 'POST',
    'data' => $query,
  ));

  return $response;
}

/**
 * Tries to guess an allowed value for the originator field for a given
 * recipient.
 */
function _send_sms_get_originator($phone = '') {
  return 'INFO_KAZ';
}

/**
 * Implements hook_menu().
 */
function send_sms_menu() {
  $items = array();

  $items['admin/config/services/send-sms'] = array(
    'title' => 'Отправка СМС',
    'description' => 'Настройка учётных данных для отправки СМС.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('send_sms_admin_config_page'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'send_sms.admin.inc',
  );

  $items['admin/config/services/send-sms/config'] = array(
    'title' => 'Настройка',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/services/send-sms/test'] = array(
    'title' => 'Проверка отправки',
    'description' => '',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('send_sms_admin_test_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'send_sms.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Loads a number of the default recipient.
 */
function send_sms_get_default_recipient() {
  return variable_get('send_sms_default_recipient', '');
}
