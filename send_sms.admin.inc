<?php

/**
 * @file
 * Admin section form builder and menu callbacks.
 */

function send_sms_admin_config_page($form, &$form_state) {
  $form['send_sms_api_username'] = array(
    '#type' => 'textfield',
    '#title' => 'Имя пользователя',
    '#description' => 'Введите имя пользователя для входа на сервер отправки СМС, которое вы получили от провайдера услуги.',
    '#required' => TRUE,
    '#default_value' => variable_get('send_sms_api_username'),
  );

  $form['send_sms_api_password'] = array(
    '#type' => 'textfield',
    '#title' => 'Пароль',
    '#description' => 'Введите пароль для входа на сервер отправки СМС, который вы получили от провайдера услуги.',
    '#required' => TRUE,
    '#default_value' => variable_get('send_sms_api_password'),
  );

  $form['send_sms_default_recipient'] = array(
    '#type' => 'textfield',
    '#title' => 'Получатель СМС по умолчанию',
    '#description' => 'На этот номер будут отправляться все СМС, если у них явно не указан другой получатель.',
    '#required' => FALSE,
    '#default_value' => variable_get('send_sms_default_recipient'),
  );

  return system_settings_form($form, $form_state);
}

function send_sms_admin_test_form($form, &$form_state) {
  $login = variable_get('send_sms_api_username');
  $password = variable_get('send_sms_api_password');
  if (empty($login) || empty($password)) {
    drupal_set_message('Учётные данные для отправки ещё не настроены.', 'error');
    return drupal_goto('admin/config/services/shop-sms');
  }

  $form['recipient'] = array(
    '#type' => 'textfield',
    '#title' => 'Номер телефона',
    '#description' => 'Введите номер телефона, на который будет отправлена тестовая СМС.',
    '#maxlength' => 24,
    '#required' => TRUE,
    '#default_value' => variable_get('send_sms_default_recipient', ''),
  );

  $form['text'] = array(
    '#type' => 'textfield',
    '#title' => 'Текст СМС',
    '#description' => 'Введите короткий текст для отправки. Неболее 10 символов.',
    '#maxlength' => 10,
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Отправить',
    ),
  );

  $form['#submit'] = array('send_sms_admin_test_form_submit');
  return $form;
}

function send_sms_admin_test_form_submit($form, &$form_state) {
  $phone = $form_state['values']['recipient'];
  $text  = $form_state['values']['text'];

  send_sms_send($phone, $text);
  drupal_set_message('СМС отправлена.');
}
